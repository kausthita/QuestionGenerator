using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QuestionPaperGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            QuestionGenerator question = new QuestionGenerator();
            Stopwatch stopwatch = Stopwatch.StartNew();
            
            if (question.Generator() != " ")
            {
                Console.WriteLine("The Question paper generated is as follows: ");
                for (int i = 0; i < 8; i++)
                {

                    Console.WriteLine("Question {0} is {1}", (i + 1), question.Generator());
                }
            }
            else
            {
                Console.WriteLine("That was all! The system has finished generating the question paper!");
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }
    }
}
